//
//  Created by Mingliang Chen on 18/3/16.
//  illuspas[a]gmail.com
//  Copyright (c) 2018 Nodemedia. All rights reserved.
//
const Logger = require('./node_core_logger');

const EventEmitter = require('events');
const { spawn } = require('child_process');

const RTSP_TRANSPORT = ['udp', 'tcp', 'udp_multicast', 'http'];
const helper = require('./helper.js');

class NodeRelaySession extends EventEmitter {
  constructor(conf) {
    super();
    this.conf = conf;
  }

  cmd_builder(program, argv) {
    for ( let i=0; i<argv.length; i++ ) {
      if ( argv[i].indexOf('&')>=0 ) argv[i] = `"${argv[i]}"`;
    }
    return program + ' ' + argv.join(' ');
  }

  run() {
    let format = this.conf.ouPath.startsWith('rtsp://') ? 'rtsp' : 'flv';
    let comment = this.conf.comment || this.conf.inPath.split("/").shift();
    let argv = ['-fflags', 'nobuffer', '-i', this.conf.inPath, '-c', 'copy', '-metadata', `comment=${comment}`, '-f', format, this.conf.ouPath];
    if (this.conf.inPath[0] === '/' || this.conf.inPath[1] === ':') {
      argv.unshift('-1');
      argv.unshift('-stream_loop');
      argv.unshift('-re');
    }

    if (this.conf.inPath.startsWith('rtsp://') && this.conf.rtsp_transport) {
      if (RTSP_TRANSPORT.indexOf(this.conf.rtsp_transport) > -1) {
        argv.unshift(this.conf.rtsp_transport);
        argv.unshift('-rtsp_transport');
      }
    }

    Logger.ffdebug(argv.toString());
    let cmd = this.cmd_builder(this.conf.ffmpeg, argv);
    console.log(cmd);
    helper.exec_in_screen(`${comment}_${this.id}`, cmd, 10);

    // this.ffmpeg_exec = spawn(this.conf.ffmpeg, argv);
    // this.ffmpeg_exec.on('error', (e) => {
    //   Logger.ffdebug(e);
    // });

    // this.ffmpeg_exec.stdout.on('data', (data) => {
    //   Logger.ffdebug(`FF输出：${data}`);
    // });

    // this.ffmpeg_exec.stderr.on('data', (data) => {
    //   Logger.ffdebug(`FF输出：${data}`);
    // });

    // this.ffmpeg_exec.on('close', (code) => {
    //   Logger.log('[Relay end] id=', this.id);
    //   this.emit('end', this.id);
    // });
  }

  end() {
    this.ffmpeg_exec.kill();
  }
}

module.exports = NodeRelaySession;
