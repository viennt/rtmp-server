const NodeMediaServer = require('./');
const config = require('./config');
const md5 = require('md5');

let streams = {};
let nms = new NodeMediaServer(config.rtmp_server)
nms.run();

nms.on('registerStream', (args) => {
  console.log('[NodeEvent on registerStream]', args);
  if ( args.id ) {
    streams[args.id] = {
      ...args,
      status: 0
    };
    setTimeout(() => {
      if ( streams[args.id] && streams[args.id].status==0 )
        delete streams[args.id];
    }, 600000) // 10 * 60 * 1000
  }
});

nms.on('preConnect', (id, args) => {
  console.log('[NodeEvent on preConnect]', `id=${id} args=${JSON.stringify(args)}`);
  // let session = nms.getSession(id);
  // session.reject();
});

nms.on('postConnect', (id, args) => {
  console.log('[NodeEvent on postConnect]', `id=${id} args=${JSON.stringify(args)}`);
});

nms.on('doneConnect', (id, args) => {
  console.log('[NodeEvent on doneConnect]', `id=${id} args=${JSON.stringify(args)}`);
});

nms.on('prePublish', (id, StreamPath, args) => {
  console.log('[NodeEvent on prePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
  let sign = md5(`${StreamPath}${config.secret}`);
  let streamId = StreamPath.split("/").pop();
  let session = nms.getSession(id);
  console.log(streams[streamId])
  console.log(sign, args['sign'])
  if ( !streams[streamId] || !streams[streamId].hasOwnProperty('timeout') || sign!=args['sign'] ) {
    session.reject();
    return;
  }
  if ( streams[streamId] ) {
    streams[streamId].status = 1;
    // if ( streams[streamId].hasOwnProperty('timeout') && streams[streamId]['timeout']>0 )
    //   setTimeout(() => {
    //     session.reject();
    //   }, parseInt(streams[streamId]['timeout'])*1000);
    if ( streams[streamId]['broadcast'] ) {
      if ( Array.isArray(streams[streamId]['broadcast']) )
        for ( let url of streams[streamId]['broadcast'] ) {
          nms.nodeEvent.emit('relayPush', url, 'live', streamId);
        }
      else if ( typeof streams[streamId]['broadcast']=="string" )
        nms.nodeEvent.emit('relayPush', streams[streamId]['broadcast'], 'live', streamId);
    }
  }
});

nms.on('postPublish', (id, StreamPath, args) => {
  console.log('[NodeEvent on postPublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
});

nms.on('donePublish', (id, StreamPath, args) => {
  console.log('[NodeEvent on donePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
  let streamId = StreamPath.split("/").pop();
  delete streams[streamId];
});

nms.on('prePlay', (id, StreamPath, args) => {
  console.log('[NodeEvent on prePlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
  // let session = nms.getSession(id);
  // session.reject();
});

nms.on('postPlay', (id, StreamPath, args) => {
  console.log('[NodeEvent on postPlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
});

nms.on('donePlay', (id, StreamPath, args) => {
  console.log('[NodeEvent on donePlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
});
