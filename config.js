module.exports = {
	rtmp_server: {
		logType: 3,
		rtmp: {
			port: 1936,
			chunk_size: 60000,
			gop_cache: true,
			ping: 30,
			ping_timeout: 60
		},
		http: {
			port: 8000,
			allow_origin: '*'
		},
		relay: {
			ffmpeg: '/usr/bin/ffmpeg'
		},
		auth: {
			api: true,
			api_user: 'admin',
			api_pass: 'admin',
			play: false,
			publish: false,
			secret: 'nodemedia2017privatekey'
		},
		// trans: {
		// 	ffmpeg: '/root/bin/ffmpeg',
		// 	tasks: [
		// 		{
		// 			app: 'live',
		// 			hls: true,
		// 			hlsFlags: '[hls_time=2:hls_list_size=3:hls_flags=delete_segments]'
		// 		}
		// 	]
		// }
	},
	secret: "^*B(H()"
}
