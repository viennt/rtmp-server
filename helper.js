const fs = require('fs');
const child_process = require('child_process');

module.exports = {
	exec_in_screen: function(screen_name, cmd, retry=0) {
		const config = `logfile ${this.current_dir()}/log/${screen_name}.log
logfile flush 1
logtstamp on
logtstamp after 1
logtstamp string "[ %t: %Y-%m-%d %c:%s ]\\012"
log on`;
		fs.writeFileSync(`/tmp/${screen_name}.conf`, config);
		if ( retry==0 )
			fs.writeFileSync(`/tmp/${screen_name}.sh`, cmd);
		else {
			fs.writeFileSync(`/tmp/${screen_name}_main.sh`, cmd); // retry 5 times
			fs.writeFileSync(`/tmp/${screen_name}.sh`, `for i in 1 2 3 4 5; do sh /tmp/${screen_name}_main.sh; sleep 1; done`);
		}
		return this.exec_shell(`screen -S ${screen_name} -c /tmp/${screen_name}.conf -dmSL sh /tmp/${screen_name}.sh`);
	},
	exec_shell: async function(cmd, debug=true) {
		if ( debug ) console.log(cmd);
		// const output = child_process.execSync(cmd);
		// return output.toString();
		return new Promise((resolve, reject) => {
			child_process.exec(cmd, {
		    	maxBuffer: 2000 * 1024
		    }, (err, stdout, stderr) => {
				if (err) {
					let stderr_str = stderr.toString();
					if ( stderr.length )
						console.log("error", {err, stdout: stdout.toString(), stderr: stderr_str})
					reject({err, stdout: stdout.toString(), stderr: stderr.toString()});
				}
				resolve(stdout.toString()+stderr.toString());
			});
		})
	},
	current_dir: function() {
		return __dirname;
	}
}
