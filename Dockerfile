FROM registry.gostream.co/streamer-base

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm i
COPY . .
EXPOSE 1936 8000
CMD ["node","app.js"]
# remember mount log/:/usr/src/app/log folder
